// @File(label="Input directory", description="Select the directory with input images", style="directory") inPath
// @File(label="Output directory", description="Select the directory to save images", style="directory") outPath
// @Boolean(label="Make Thumbnails?", description="Create a copy of the image at 300x300 pixels") doThumb
// @String(label="Logo URL", value="http://pcwww.liv.ac.uk/~dnmason/logo.png", persist=false) url
//
//	Script to watermark and thumbnail images for use on a webgallery or similar.
//
//												Dave Mason 
//												Centre for Cell Imaging, Liverpool
//												Feb 2018
//
//-- RUN OPTIONS:
setBatchMode(true);
verbose=1;

//--------------------------------//------------------------------------
//-- PART 1: Get file list and specify arrays
//--------------------------------//------------------------------------
saveSettings();
close("*");
dirList = getFileList(inPath);
if (verbose>0){Array.print(dirList);}
//-- Sanity check, dirList will always have at least one entry
for (i=0; i<dirList.length; i++){

if (matches(dirList[i], "(?i).*\\.(png|jpe?g|jfif)$")){
//-- check requirements for brackets here (to deal with filenames containing spaces)
open(inPath+File.separator+dirList[i]);
if (verbose>0){	print("-------------------------------------");print("In File: "+inPath+File.separator+dirList[i]);	}

//-- work out outfile
outFile=substring(dirList[i], 0, lastIndexOf(dirList[i], "."))+".png";
if (verbose>0){print("OutFile: "+outFile);}

//-- get properties
title=getTitle();
if (verbose>0){print("Title: "+title);}
getDimensions(w, h, c, s, f);
if (verbose>0){print("Image Size: "+w+"x"+h);}
scaleSize=floor(((w*0.15)+(h*0.15))/2);
if (verbose>0){print("Logo size: "+scaleSize+" px");}
//-- pull in logo if required
if (isOpen("logo.png")==false){
open(url);
}
selectWindow("logo.png");
//-- scale the logo
run("Scale...", "x=- y=- width="+scaleSize+" height="+scaleSize+" interpolation=Bilinear average create");

//-- now make the logo image the same size as original by padding black in two steps
run("Canvas Size...", "width="+(scaleSize*1.2)+" height="+(scaleSize*1.2)+" position=Center zero");
run("Select None");
run("Canvas Size...", "width="+w+" height="+h+" position=Bottom-Right zero");

run("Copy");
selectWindow(title);
setPasteMode("Transparent-zero");
run("Paste");
setPasteMode("Copy");
run("Select None");
saveAs("PNG", outPath+File.separator+outFile);

//-- Now make a thumbnail for the gallery
if (doThumb==true){
//-- check for squareness
getDimensions(w, h, c, s, f);
if (w>h){
	run("Canvas Size...", "width="+w+" height="+w+" position=Center");
}
if (w<h){
	run("Canvas Size...", "width="+h+" height="+h+" position=Center");
}

//-- scale to 300px
run("Scale...", "x=- y=- width=300 height=300 interpolation=Bilinear average create");
saveAs("PNG", outPath+File.separator+replace(outFile,".png","_T.png"));

} //-- close thumbnail
close("*");

}//-- close endswith check

} //-- Close image Loop

restoreSettings(); //-- restore original settings
setBatchMode(false);
print("-------------------------------------");print("  Finished");