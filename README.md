### Watermarking images ###
A simple [Fiji](http://fiji.sc) script to watermark a directory of PNG, JPG, JPEG, JFIF files with a logo found 
at a URL. 

The logo will be scaled to a percentage of the average width and height of the original and overlain in the bottom 
right corner with some padding.

![watermark](example.png)

Optionally, a square thumbnail image can be created at 300x300 pixels. Non square images will be white padded.

### Acknowledgement ###
written by [Dave Mason](http://pcwww.liv.ac.uk/~dnmason), from the University of Liverpool [Centre for Cell 
Imaging](http://cci.liv.ac.uk).
